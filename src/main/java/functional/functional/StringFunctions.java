package functional.functional;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

public class StringFunctions {
    public static void main(String[] args) {
        UnaryOperator<String> quote = text -> "\"" + text + "\"";
        UnaryOperator<String> addMark = text -> text + "!";
        System.out.println(quote.apply("Hola"));
        System.out.println(addMark.apply("Hola"));

        BiFunction<Integer, Integer, Integer> multiplicacion =
                (x, y) -> x * y;
        System.out.println(multiplicacion.apply(5,6));

        BinaryOperator<Integer> multi2 = (x, y) -> x * y;
        System.out.println(multi2.apply(6,7));

        BiFunction<String, Integer, String> leftPad =
                (text, number) -> String.format("%" + number + "s",text);

        System.out.println(leftPad.apply("Luis Camilo", 20));

        List<BiFunction<String, Integer, String>> formateadores;
    }
}
